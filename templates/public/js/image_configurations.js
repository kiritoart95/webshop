/**
 * Created by Erlan on 1/21/17.
 */

function fix_images(path) {
    var images = $(path);


    function ParseURLAndGetLastPath(url) {
        var arrayOfStrings;
        arrayOfStrings = url.split('/');

        return arrayOfStrings[arrayOfStrings.length - 1];
    }

    $(images).each(setsize);

    function setsize() {
        var img = $(this),
            img_dom = img.get(0),
            container;

        container = img.parent();

        if (img_dom.complete) {
            resize();
        } else img.one('load', resize);

        function resize() {
            if ((container.width() / container.height()) < (img_dom.width / img_dom.height)) {
                img.width('100%');
                img.height('auto');
                return;
            }
            img.height('100%');
            img.width('auto');
        }
    }
}

$(document).ready(function () {
    $(window).on('resize', fix_images('.pr-elements > img'));
    $(window).on('resize', fix_images('.product-img > img'));
    fix_images('.pr-elements > img');
    fix_images('.product-img > img');
});
