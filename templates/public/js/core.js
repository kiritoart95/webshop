/**
 * Created by erlan on 1/8/17.
 */

function initCountButtons() {
    $('.plus').each(function (i, obj) {
        $(obj).on('click', function (event) {
            event.preventDefault();
            var input = $(obj).parent().parent().find('input[type="number"]');
            var value = parseInt($(input).val());
            ++value;
            $(input).val(value);
            $(input).trigger('change');
        });
    });

    $('.minus').each(function (i, obj) {
        $(obj).on('click', function (event) {
            event.preventDefault();
            var input = $(obj).parent().parent().find('input[type="number"]');
            var value = parseInt($(input).val());
            if (value > 1) {
                --value;
            }
            $(input).val(value);
            $(input).trigger('change');
        })
    });
}

function initBasketButtons() {
    var select = $('div.weight-paint > select');
    var inputCount = $('.quantity-products.quant');
    var removeButton = $('.remove-order');

    $(inputCount).on('input', function (event) {
        $(this).trigger('change');
    });

    $(removeButton).each(function (e, obj) {
        $(obj).unbind().bind('click', function (event) {
            event.preventDefault();
            var parent = $(obj).parent().parent().parent();
            var weight = $('.data-product', $(parent)).data('type') == 'one' ? $('.data-product', $(parent)).find('span').data('id') : $('.data-product', $(parent)).find('form > div > select').val();
            var order_number = $(parent).attr('data-order-number');
            $.ajax({
                url: $(removeButton).attr('data-remove-id'),
                method: 'POST',
                dataType: 'JSON',
                data: {'csrfmiddlewaretoken': csrfMiddleWareToken, 'weight': weight, 'order_number': order_number},
                success: function (response) {
                    if (response.success) {
                        $(parent).fadeOut('slow', function () {
                            $(parent).remove();
                        });
                        recountBasketTotalPrice();
                        var cartProductCount = $('.sum-goods');
                        var productCount = parseInt($(cartProductCount).html());
                        --productCount;
                        correctFinish(productCount, cartProductCount);
                    }
                }
            });
        })
    });

    $(select).on('change', function (event) {
        var row = $(this).parent().parent().parent().parent();
        $(row).find('.row-price > span').html($('option:selected', $(this)).data('price') + ' ₽');
        /*console.log(
         'url: ' + $(row).attr('data-update') + '\n' +
         'csrf: ' + '' + '\n' +
         'weight: ' + $('option:selected', $(this)).val() + '\n' +
         'count: ' +  $(row).find('.row-count > form > input').val()
         );*/
        var url = $(row).attr('data-update');
        var weight = $('option:selected', $(this)).val();
        var rowCount = $(row).find('.row-count > form > input').val();
        var orderNumber = $(row).attr('data-order-number');
        updateBasket(url, orderNumber, csrfMiddleWareToken, weight, rowCount);
    });

    $(inputCount).on('change', function (event) {
        var row = $(this).parent().parent().parent();
        var url = $(row).attr('data-update');
        var weight = $('.data-product', $(row)).data('type') == 'one' ? $('.data-product', $(row)).find('span').data('id') : $('.data-product', $(row)).find('form > div > select').val();
        var rowCount = $(this).val();
        var orderNumber = $(row).attr('data-order-number');

        updateBasket(url, orderNumber, csrfMiddleWareToken, weight, rowCount);
    });
}

function initAllAddToCartButtons() {

    var button = $('.button.trigger');
    var func = function (event) {
        event.preventDefault();

        var data = {
            'csrfmiddlewaretoken': csrfMiddleWareToken,
            'count': $(this).parent().find('input[type="number"]').val(),
            'weight': $(this).attr('data-type') == 'one' ? $(this).parent().parent().find('span.weight').data('id') : $(this).parent().parent().find('.weight-paint select').val()
        };

        /*for(var item in data) {
         if(data.hasOwnProperty(item)) {
         console.log(item + ':' + data[item]);
         }
         }*/

        $.ajax({
            url: $(this).attr('data-target'),
            method: 'POST',
            dataType: 'JSON',
            data: data,
            success: function (response) {
                if (response.success) {
                    $.ajax({
                        url: listBasketItems,
                        dataType: 'HTML',
                        method: 'POST',
                        data: {'csrfmiddlewaretoken': csrfMiddleWareToken},
                        success: function (response) {
                            $('.basket-rows').html(response);
                            $('.modal-wrapper').toggleClass('open');
                            initCountButtons();
                            recountBasketTotalPrice();
                            var cartProductCount = $('.sum-goods');
                            var productCount = parseInt($(cartProductCount).html());
                            ++productCount;
                            correctFinish(productCount, cartProductCount);
                        },
                        error: function () {
                            console.log('Error!!!');
                        }
                    });
                }
            },
            error: function () {
                console.log('Error!');
            }
        });
    };
    $(button).unbind('click').bind('click', func);
}

function correctFinish(productCount, cartProductCountObject) {
    var word = 'товар';
    if (productCount + ''[productCount + ''.length - 1] == '1') {
        word += '';
    } else if (productCount > 1 && productCount <= 4) {
        word += 'а';
    } else {
        word += 'ов';
    }
    $(cartProductCountObject).html(productCount);
    $('.sum-goods-word').html(word);
}

function initAllSelects() {
    var weightPaint = $('.weight-paint');

    $(weightPaint).each(function (i, obj) {
        var mySelect = $(obj).find('select');
        $(mySelect).on('change', function (event) {
            $(obj).parent().attr('data-price', $('option:selected', $(mySelect)).attr('data-price'))
            $(obj).parent().find('span.price').html($('option:selected', $(mySelect)).attr('data-price') + ' ₽');
        })
    });
}

function recountBasketTotalPrice() {
    $.ajax({
        url: recountTotalPriceUrl,
        method: 'POST',
        dataType: 'JSON',
        data: {'csrfmiddlewaretoken': csrfMiddleWareToken},
        success: function (response) {
            var location = document.location.href.split('/');
            var currentLocation = location[location.length - 1];
            if (currentLocation == 'basket')
                $('#basket-total-price').html(response.result + ' ₽');
            $('.total-amount').html(response.result + ' ₽');
        },
        error: function () {
            console.error('Error to recount basket!');
        }
    });
}

function updateBasket(url, order_number, csrf, weight, count) {
    var data = {
        'csrfmiddlewaretoken': csrf,
        'weight': weight,
        'count': count,
        'order_number': order_number
    };
    $.ajax({
        url: url,
        method: 'POST',
        dataType: 'JSON',
        data: data,
        success: function (response) {
            if (response.success) {
                recountBasketTotalPrice();
            } else {
                console.error('Something went wrong when updating basket...');
            }
        },
        error: function () {
            console.error('Something went wrong when updating basket...');
        }
    });
}

function initTradePage() {
    var $ = function (selector) {
        return document.querySelectorAll(selector);
    };


    // Define tabs, write down them classes
    var tabs = [
        '.tabbed-section__selector-tab-1',
        '.tabbed-section__selector-tab-2',
        '.tabbed-section__selector-tab-3'
    ];

    // Create the toggle function
    var toggleTab = function (element) {
        // var parent = element.parentNode;

        // Do things on click
        $(element)[0].addEventListener('click', function () {
            var tabSectionOne = '.tabbed-section-1';
            var tabSectionTwo = '.tabbed-section-2';
            var tabSectionThree = '.tabbed-section-3';

            this.parentNode.childNodes[1].classList.remove('active');
            this.parentNode.childNodes[3].classList.remove('active');
            this.parentNode.childNodes[5].classList.remove('active');

            // Then, give `this` (the clicked tab), the active class
            this.classList.add('active');

            // Check if the clicked tab contains the class of the 1 or 2
            if (this.classList.contains('tabbed-section__selector-tab-1')) {
                // and change the classes, show the first content panel
                $(tabSectionOne)[0].classList.remove('hidden');
                $(tabSectionOne)[0].classList.add('visible');

                // Hide the second
                $(tabSectionTwo)[0].classList.remove('visible');
                $(tabSectionTwo)[0].classList.add('hidden');
                $(tabSectionThree)[0].classList.remove('visible');
                $(tabSectionThree)[0].classList.add('hidden');
            }

            if (this.classList.contains('tabbed-section__selector-tab-2')) {
                // and change the classes, show the second content panel
                $(tabSectionTwo)[0].classList.remove('hidden');
                $(tabSectionTwo)[0].classList.add('visible');
                // Hide the first
                $(tabSectionOne)[0].classList.remove('visible');
                $(tabSectionOne)[0].classList.add('hidden');
                $(tabSectionThree)[0].classList.remove('visible');
                $(tabSectionThree)[0].classList.add('hidden');
            }

            if (this.classList.contains('tabbed-section__selector-tab-3')) {
                // and change the classes, show the second content panel
                $(tabSectionThree)[0].classList.remove('hidden');
                $(tabSectionThree)[0].classList.add('visible');
                // Hide the first
                $(tabSectionOne)[0].classList.remove('visible');
                $(tabSectionOne)[0].classList.add('hidden');
                $(tabSectionTwo)[0].classList.remove('visible');
                $(tabSectionTwo)[0].classList.add('hidden');
            }
        });
    };

    for (var i = tabs.length - 1; i >= 0; i--) {
        toggleTab(tabs[i]);
    }
}

function initTradeBasketButtons() {
    var productWeightSelect = $('#product-weight');

    var addToCart = function (event) {
        event.preventDefault();

        var data = {
            'csrfmiddlewaretoken': csrfMiddleWareToken,
            'weight': $('.product_cost-elements').attr('data-product-type') == 'many' ? parseInt($(productWeightSelect).val()) : parseInt($('#product-weight').attr('data-weight-id')),
            'count': $('input[name="quantity"]').val()
        };

        for(var item in data) {
           console.log(data[item]);
        }

        $.ajax({
            url: $(this).attr('data-target'),
            method: 'POST',
            data: data,
            dataType: 'JSON',
            success: function (response) {
                if (response.success) {
                    $.ajax({
                        url: listBasketItems,
                        dataType: 'HTML',
                        method: 'POST',
                        data: {'csrfmiddlewaretoken': csrfMiddleWareToken},
                        success: function (response) {
                            $('.basket-rows').html(response);
                            $('.modal-wrapper').toggleClass('open');
                            initCountButtons();
                        },
                        error: function () {
                            console.log('Error!!!');
                        }
                    });
                }
            },
            error: function () {
                console.log('error!');
            }
        });
    };

    $(document).on('click', '.button.trigger', addToCart);

   // $('.button.trigger').unbind('click').bind('click', addToCart);

    $(productWeightSelect).on('change', function (event) {
        $('#product-price').html($('option:selected', $(this)).data('price') + ' ₽');
        var discount = parseInt($('.product-element').data('discount'));
        if (discount > 0) {
            var discountPrice = parseInt($('option:selected', $(this)).data('price')) - (parseFloat($('option:selected', $(this)).data('price')) * discount) / 100;
            $('.sale-price').html(discountPrice + ' ₽');
        }
    });
}