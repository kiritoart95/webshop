from django.contrib import admin

# Register your models here.
from app_models.models import Category
from django.contrib.auth.models import User as BaseUser, Group as BaseGroup


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['title', 'parent_category', 'type']
    list_filter = ['title', 'parent_category', ]

admin.site.register(Category, CategoryAdmin)

from app_models.models import Trade


class TradeAdmin(admin.ModelAdmin):
    list_display = ['title', 'discount', 'is_visible', 'category', 'brand']
    list_filter = ['title', 'discount', 'is_visible', 'category', 'brand']
    list_editable = ['is_visible']

admin.site.register(Trade, TradeAdmin)

from app_models.models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ['username', 'first_name', 'last_name']
    list_filter = ['username', 'first_name', 'last_name']

admin.site.register(User, UserAdmin)

admin.site.unregister(BaseUser)


from app_models.models import Order


class OrderAdmin(admin.ModelAdmin):
    list_display = ['elements', 'sum', 'created_at', 'accepted', 'delivered']
    list_filter = ['sum', 'created_at']

admin.site.register(Order, OrderAdmin)

from app_models.models import News


class NewsAdmin(admin.ModelAdmin):
    list_display = ['title', 'description', 'is_visible', 'created_at']
    list_filter = ['is_visible', 'created_at']
    list_editable = ['is_visible']

admin.site.register(News, NewsAdmin)

from app_models.models import PriceWeight


class PriceWeightAdmin(admin.ModelAdmin):
    list_display = ['price', 'weight']
    list_filter = ['price', 'weight']

admin.site.register(PriceWeight, PriceWeightAdmin)


from app_models.models import PageContent


admin.site.register(PageContent)
