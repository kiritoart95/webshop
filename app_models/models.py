# coding=utf-8
from __future__ import unicode_literals
from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import User as BaseUser
from django.db import models

# Create your models here.


class Category(models.Model):
    class Meta:
        verbose_name_plural = 'Категории'
        verbose_name = 'Категория'

    title = models.CharField(verbose_name='Название', max_length=255, unique=False, null=False)
    parent_category = models.ForeignKey(verbose_name='Родительская категория', to='Category', on_delete=models.CASCADE,
                                        null=True, blank=True)
    type = models.CharField(verbose_name='Раздел категории', choices=(
        ('construction', 'Строительные материалы'), ('decoration', 'Отделочные материалы'),
        ('tools', 'Инструменты')
    ), max_length=255, null=True)
    description = models.CharField(max_length=255, verbose_name='Описание категории', null=True, blank=True)

    created_at = models.DateTimeField(verbose_name='Создано', auto_now_add=True, null=True)
    updated_at = models.DateTimeField(verbose_name='Обновлено', auto_now=True, null=True)

    def __unicode__(self):
        return self.title


class Trade(models.Model):
    class Meta:
        db_table = 'trade'
        verbose_name_plural = 'Товары'
        verbose_name = 'Товар'

    article = models.CharField(verbose_name='Артикул', max_length=255, null=True, blank=True)
    title = models.CharField(verbose_name='Название', max_length=255, unique=False, null=False)
    description = RichTextUploadingField(verbose_name='Описание', null=True, blank=True)
    short_description = RichTextUploadingField(verbose_name='Краткое описание', null=True, blank=True)
    instruction = RichTextUploadingField(verbose_name='Инструкция', null=True, blank=True)
    properties = RichTextUploadingField(verbose_name='Свойства', null=True, blank=True)
    price_settings = models.ManyToManyField('PriceWeight', verbose_name='Цена')
    is_visible = models.BooleanField(verbose_name='Видим?', null=False)
    category = models.ForeignKey(verbose_name='Категория', to=Category, on_delete=models.CASCADE, null=True)
    discount = models.IntegerField(verbose_name='Скидка', default=0)
    image = models.ImageField(verbose_name='Изображение', null=True, blank=True, upload_to='images')
    brand = models.CharField(verbose_name='Бренд', max_length=255, choices=(
        ('alinex', 'Alinex'), ('nashi', 'Наши'), ('alinapaint', 'Alina Paint')
    ), null=True)
    consumption = models.FloatField(verbose_name='Расход (на единицу кв.м.)', default=0)
    layer_weight = models.FloatField(verbose_name='Расходная толщина (мм.)', default=0)
    product_code = models.CharField(verbose_name='Код товара', max_length=255, null=True, blank=True)

    created_at = models.DateTimeField(verbose_name='Создано', auto_now_add=True, null=True)
    updated_at = models.DateTimeField(verbose_name='Об  новлено', auto_now=True, null=True)

    def __unicode__(self):
        return self.title


class User(BaseUser):
    class Meta:
        verbose_name = 'Пользователи'
        verbose_name_plural = 'Пользователи'


class Order(models.Model):
    class Meta:
        verbose_name_plural = 'Заказы'
        verbose_name = 'Заказ'

    elements = models.TextField(verbose_name='Элементы заказа', null=False, default='Заказы:')
    sum = models.FloatField(verbose_name='Сумма', null=False)
    accepted = models.BooleanField(verbose_name='Принято', default=False)
    delivered = models.BooleanField(verbose_name='Доставлено', default=False)
    
    created_at = models.DateTimeField(verbose_name='Создано', auto_now_add=True, null=True)
    updated_at = models.DateTimeField(verbose_name='ОБновлено', auto_now=True, null=True)

    def __unicode__(self):
        return '%s - %s' % (self.user.username, self.sum)


class OrderItem(models.Model):
    class Meta:
        verbose_name_plural = 'Элементы заказов'
        verbose_name = 'Элемент заказа'

    trade = models.ForeignKey(verbose_name='Товар', to=Trade, on_delete=models.CASCADE, null=False)
    quantity = models.IntegerField(verbose_name='Количество', null=False)
    order = models.ForeignKey(verbose_name='Заказ', to=Order, on_delete=models.CASCADE, null=False)
    
    created_at = models.DateTimeField(verbose_name='Создано', auto_now_add=True, null=True)
    updated_at = models.DateTimeField(verbose_name='Обновлено', auto_now=True, null=True)

    def __unicode__(self):
        return '%s - %s' % (self.trade.title, self.quantity)


class News(models.Model):
    class Meta:
        verbose_name_plural = 'Новости'
        verbose_name = 'Новости'

    title = models.CharField(verbose_name='Заголовок', max_length=255, unique=False, null=False)
    description = RichTextUploadingField(verbose_name='Описание категории', null=True, blank=True)
    is_visible = models.BooleanField(verbose_name='Видим?', null=False)
    
    created_at = models.DateTimeField(verbose_name='Создано', auto_now_add=True, null=True)
    updated_at = models.DateTimeField(verbose_name='Обновлено', auto_now=True, null=True)

    def __unicode__(self):
        return self.title


class PriceWeight(models.Model):
    class Meta:
        verbose_name_plural = 'Цены'
        verbose_name = 'Цена'

    weight = models.CharField(verbose_name='Вес', max_length=255, unique=False, null=False)
    price = models.FloatField(verbose_name='Цена', null=False)
    
    created_at = models.DateTimeField(verbose_name='Создано', auto_now_add=True, null=True)
    updated_at = models.DateTimeField(verbose_name='Обновлено', auto_now=True, null=True)

    def __unicode__(self):
        return '%s кг - %s руб' % (self.weight, self.price)


class PageContent(models.Model):
    class Meta:
        db_table = 'page_content'
        verbose_name = 'Редактрование страниц'
        verbose_name_plural = 'Редактирование страниц'

    about = RichTextUploadingField(verbose_name='О нас', null=True, blank=True)
    contacts = RichTextUploadingField(verbose_name='Контакты', null=True, blank=True)
    alinex = RichTextUploadingField(verbose_name='Alinex', null=True, blank=True)
    alina_paint = RichTextUploadingField(verbose_name='Alina Paint', null=True, blank=True)
    nashi = RichTextUploadingField(verbose_name='Nashi', null=True, blank=True)

    def __unicode__(self):
        return 'Редактирование страниц: "О нас", "Контакты" и страница брендов'
