# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-12-28 08:54
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_models', '0003_order_orderitem'),
    ]

    operations = [
        migrations.AddField(
            model_name='trade',
            name='discount',
            field=models.IntegerField(default=0, verbose_name='\u0421\u043a\u0438\u0434\u043a\u0430'),
        ),
    ]
