"""webshop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from webshop import settings
from app.views import *

urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
    url(r'^admin/', admin.site.urls),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url(r'^$', index, name='index'),
    url(r'^news$', news, name='news'),
    url(r'^search$', search, name='search'),
    url(r'^about$', about, name='about'),
    url(r'^brands$', brands, name='brands'),
    url(r'^contacts$', contacts, name='contacts'),
    url(r'^alinex$', alinex, name='alinex'),
    url(r'^alinapaint$', alina_paint, name='alina_paint'),
    url(r'^nashi$', nashi, name='nashi'),
    url(r'^trade/(?P<id>\d+)$', trade, name='trade'),
    url(r'^basket$', basket, name='basket'),
    url(r'^basket/add/(?P<trade_id>\d+)$', add_to_basket, name='add_to_basket'),
    url(r'^basket/remove$', remove_from_basket, name='remove_from_basket'),
    url(r'^basket/update/(?P<trade_id>\d+)$', update_basket, name='update_basket'),
    url(r'^basket/all$', list_basket_items, name='list_basket_items'),
    url(r'^basket/clear$', clear_basket, name='clear_basket'),
    url(r'^basket/count/total/price/$', count_basket_cost, name='count_basket_total_price'),
    url(r'^basket/pay/prepare/$', payment_preparing, name='payment_preparing'),
    url(r'^basket/pay/cash/$', pay_money, name='payment_cash'),
    url(r'^basket/pay/result$', payment_preparing, name='payment_preparing'),
    url(r'^calculator/category/(?P<category_id>\d+)/$', calculator, name='calculator'),
    url(r'^category/(?P<category_id>\d+)/$', category, name='category'),
    url(r'^category/(?P<category_id>\d+)/(?P<sub_category_id>\d+)/$', category, name='sub_category'),
    url(r'^misc/trade/add/$', temp_trade_add_view, name='temp_trade_add'),
    url(r'^upload/trade$', upload_trade_data, name='upload_trade_data'),
    url(r'^ajax/index/paginate$', ajax_index_pagination, name='index_paginate'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
