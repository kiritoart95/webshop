# coding=utf-8
import datetime
import json

import threading

from django.core.mail import EmailMessage
from django.core.paginator import Paginator
from django.shortcuts import render, render_to_response
from django.http import JsonResponse, HttpResponse, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
# from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import csrf_exempt

from webshop import settings

from app_models.models import *
from form import *

# Create your views here.


def generate_view_params(request):
    if settings.SESSION_COOKIE_BASKET_NAME in request.COOKIES:
        cart_items = json.loads(request.COOKIES[settings.SESSION_COOKIE_BASKET_NAME])
    else:
        cart_items = []

    total_sum = 0
    if len(cart_items) > 0:
        for item in cart_items:
            total_sum += int(item['price']) * int(item['count'])

    params = {
        'basket_items_count': len(cart_items),
        'basket_total_price': total_sum,
        'search_form': SearchForm(request.POST)
    }

    return params


def get_categories():
    constructions = Category.objects.filter(parent_category=None, type='construction')
    decorations = Category.objects.filter(parent_category=None, type='decoration')
    tools = Category.objects.filter(parent_category=None, type='tools')

    params = dict(
        constructions=constructions, decorations=decorations, tools=tools
    )
    return params


def index(request):
    trades_with_discounts = Trade.objects.filter(discount__gt=0, is_visible=True)
    paginator = Paginator(Trade.objects.filter(discount=0, is_visible=True).order_by('-created_at'), 20)
    new_trades = paginator.page(1)
    params = dict(
        trades_with_discounts=trades_with_discounts, new_trades=new_trades
    )
    params.update(generate_view_params(request))
    params.update(get_categories())
    return render(request, 'app/index.html', params)


def ajax_index_pagination(request):
    paginator = Paginator(Trade.objects.filter(discount=0, is_visible=True).order_by('-created_at'), 20)
    new_trades = paginator.page(request.POST.get('page', 1))

    return render_to_response('app/partial/_trade_list.html', {'new_trades': new_trades})


def calculator(request, category_id):
    form = CalculatorForm(request.POST)
    params = {
        'calculating_form': form,
        'category_id': category_id
    }

    if request.POST:
        if form.is_valid():
            area_value = form.cleaned_data['total_sum']
            category = Category.objects.get(id=category_id)
            products = Trade.objects.filter(category=category, is_visible=True)
            items = list()
            for item in products:
                item_count = float(area_value) % float(item.consumption)
                if item_count > 0:
                    item_count = ternary_operate(item.consumption > 0, (area_value / item.consumption) + 1, 0)
                else:
                    item_count = ternary_operate(item.consumption > 0, (area_value / item.consumption), 0)

                new_item = dict(
                    id=item.id, title=item.title, description=item.description, short_description=item.short_description,
                    instruction=item.instruction, properties=item.properties, price_settings=item.price_settings,
                    is_visible=item.is_visible, category=item.category, discount=item.discount, image=item.image,
                    brand=item.brand, consumption=item.consumption, layer_weight=item.layer_weight,
                    created_at=item.created_at, updated_at=item.updated_at, item_count=int(item_count)
                )
                items.append(new_item)

            params['new_trades'] = items

    params.update(generate_view_params(request))
    params.update(get_categories())
    return render(request, 'app/calculator.html', params)


def category(request, category_id, sub_category_id=None):
    if request.GET.get('brand'):
        brand = request.GET.get('brand')
    else:
        brand = None

    _category = Category.objects.filter(id=category_id).first()
    if brand:
        products = Trade.objects.filter(category=_category, brand=brand)
    else:
        products = Trade.objects.filter(category=_category)
    params = {
        'category': _category,
        'new_trades': products
    }
    if sub_category_id is not None:
        sub_category = Category.objects.filter(parent_category=_category).first()
        if brand:
            products = Trade.objects.filter(category=sub_category, brand=brand)
        else:
            products = Trade.objects.filter(category=sub_category)
        params['new_trades'] = products
        params['sub_category'] = sub_category

    params.update(generate_view_params(request))
    params.update(get_categories())
    return render(request, 'app/category.html', params)


def basket(request):
    if settings.SESSION_COOKIE_BASKET_NAME in request.COOKIES:
        cart_items = json.loads(request.COOKIES[settings.SESSION_COOKIE_BASKET_NAME])
    else:
        cart_items = []

    total_sum = 0
    if len(cart_items) > 0:
        for item in cart_items:
            product = Trade.objects.get(id=item['id'])
            price = ternary_operate(product.discount > 0,
                                    (product.price_settings.first().price - ((product.price_settings.first().price * product.discount) / 100)),
                                    product.price_settings.first().price)
            cart_items[cart_items.index(item)]['weight'] = product.price_settings
            if product.discount != 0:
                cart_items[cart_items.index(item)]['product'] = product
            total_sum += price * int(item['count'])

    params = {
        'basket_items': cart_items,
        'total_price': total_sum
    }
    params.update(generate_view_params(request))
    return render(request, 'app/basket.html', params)


def news(request):
    params = {

    }
    params.update(generate_view_params(request))
    params.update(get_categories())
    return render(request, 'app/news.html', params)


def search(request):
    params = dict()
    if request.POST:
        search_form = SearchForm(request.POST)
        if search_form.is_valid():
            key_word = search_form.cleaned_data['title']
            products = Trade.objects.filter(title__icontains=key_word.lower()).order_by('-created_at')
            params['new_trades'] = list(products)

    params.update(generate_view_params(request))
    params.update(get_categories())
    return render(request, 'app/search.html', params)


def about(request):
    page_content = PageContent.objects.all().first()
    params = {
        'content': page_content.about
    }
    params.update(generate_view_params(request))
    return render(request, 'app/about.html', params)


def brands(request):
    params = {

    }
    params.update(generate_view_params(request))
    return render(request, 'app/brands.html', params)


def contacts(request):
    page_content = PageContent.objects.all().first()
    params = {
        'content': page_content.contacts
    }
    params.update(generate_view_params(request))
    params.update(get_categories())
    return render(request, 'app/contacts.html', params)


def alinex(request):
    page_content = PageContent.objects.all().first()
    params = {
        'content': page_content.alinex
    }
    params.update(generate_view_params(request))
    return render(request, 'app/brands/alinex.html', params)


def alina_paint(request):
    page_content = PageContent.objects.all().first()
    params = {
        'content': page_content.alina_paint
    }
    params.update(generate_view_params(request))
    return render(request, 'app/brands/alinapaint.html', params)


def nashi(request):
    page_content = PageContent.objects.all().first()
    params = {
        'content': page_content.nashi
    }
    params.update(generate_view_params(request))
    return render(request, 'app/brands/nashi.html', params)


def trade(request, id):
    trade = Trade.objects.get(id=id)
    params = {
        'trade': trade
    }
    params.update(generate_view_params(request))
    return render(request, 'app/trade.html', params)


def set_cookie(response, key, value, days_expire=365):
    if days_expire is None:
        max_age = 365 * 24 * 60 * 60  # one year
    else:
        max_age = days_expire * 24 * 60 * 60
    expires = datetime.datetime.strftime(datetime.datetime.utcnow() + datetime.timedelta(seconds=max_age),
                                         "%a, %d-%b-%Y %H:%M:%S GMT")
    response.set_cookie(key, value, max_age=max_age, expires=expires, domain=settings.SESSION_COOKIE_DOMAIN,
                        secure=settings.SESSION_COOKIE_SECURE or None)
    return response


def ternary_operate(condition, first, second):
    if condition:
        return first
    else:
        return second


def add_to_basket(request, trade_id):
    if request.is_ajax():
        if settings.SESSION_COOKIE_BASKET_NAME in request.COOKIES:
            cart_items = json.loads(request.COOKIES[settings.SESSION_COOKIE_BASKET_NAME])
        else:
            cart_items = []

        product = Trade.objects.get(id=trade_id)
        weight_item = PriceWeight.objects.get(id=int(request.POST.get('weight')))
        count = int(request.POST.get('count'))
        order_number = len(cart_items) + 1
        price = ternary_operate(product.discount > 0,
                                (weight_item.price - ((weight_item.price * product.discount) / 100)),
                                weight_item.price)
        data = dict(
            id=product.id,
            title=product.title,
            weight=int(weight_item.weight),
            count=count,
            price=price,
            weight_id=weight_item.id
        )

        is_new = True
        if len(cart_items) > 0:
            for item in cart_items:
                if item['id'] == data['id'] and item['weight'] == data['weight']:
                    placeholder = cart_items[cart_items.index(item)]
                    data['count'] += placeholder['count']
                    cart_items[cart_items.index(item)] = data
                    is_new = False

        if is_new:
            data['order_number'] = order_number
            cart_items.append(data)

        result = JsonResponse(dict(success=True))
        result = set_cookie(result, settings.SESSION_COOKIE_BASKET_NAME, json.dumps(cart_items))
    else:
        result = HttpResponseRedirect(reverse('index'))

    return result


def list_basket_items(request):
    if request.is_ajax():
        if settings.SESSION_COOKIE_BASKET_NAME in request.COOKIES:
            cart_items = json.loads(request.COOKIES[settings.SESSION_COOKIE_BASKET_NAME])
        else:
            cart_items = []

        if len(cart_items) > 0:
            for item in cart_items:
                product = Trade.objects.get(id=item['id'])
                cart_items[cart_items.index(item)]['weight'] = product.price_settings
                if product.discount > 0:
                    cart_items[cart_items.index(item)]['product'] = product

        response = render_to_response('app/partial/_basket_row.html', {'basket_items': cart_items})
        return response

    return HttpResponse('')


def clear_basket(request):
    response = JsonResponse(dict(success=True))
    response = set_cookie(response, settings.SESSION_COOKIE_BASKET_NAME, [])

    return response


def remove_from_basket(request, ):
    if request.is_ajax():
        if settings.SESSION_COOKIE_BASKET_NAME in request.COOKIES:
            cart_items = json.loads(request.COOKIES[settings.SESSION_COOKIE_BASKET_NAME])
        else:
            cart_items = []

        if len(cart_items) > 0:
            for item in cart_items:
                if item['order_number'] == int(request.POST.get('order_number')):
                    cart_items.remove(item)
                    break

        response = JsonResponse(dict(success=True))
        response = set_cookie(response, settings.SESSION_COOKIE_BASKET_NAME, json.dumps(cart_items))

        return response
    else:
        return JsonResponse(dict(success=False))


def update_basket(request, trade_id):
    if request.is_ajax():
        if settings.SESSION_COOKIE_BASKET_NAME in request.COOKIES:
            cart_items = json.loads(request.COOKIES[settings.SESSION_COOKIE_BASKET_NAME])
        else:
            cart_items = []

        order_number = int(request.POST.get('order_number'))
        product = Trade.objects.get(id=trade_id)
        weight_item = PriceWeight.objects.get(id=int(request.POST.get('weight')))
        count = int(request.POST.get('count'))
        price = ternary_operate(product.discount > 0, (weight_item.price * product.discount) / 100, weight_item.price)

        data = dict(
            id=product.id,
            title=product.title,
            weight=int(weight_item.weight),
            count=count,
            price=price,
            order_number=order_number,
            weight_id=weight_item.id
        )

        if len(cart_items) > 0:
            for item in cart_items:
                if item['order_number'] == data['order_number']:
                    cart_items[cart_items.index(item)] = data
                    break

        result = JsonResponse(dict(success=True))
        result = set_cookie(result, settings.SESSION_COOKIE_BASKET_NAME, json.dumps(cart_items))
    else:
        result = HttpResponseRedirect(reverse('index'))

    return result


def count_basket_cost(request, inside=False):
    total_price = 0
    if request.is_ajax():
        cart_item = []
        if settings.SESSION_COOKIE_BASKET_NAME in request.COOKIES:
            cart_item = json.loads(request.COOKIES[settings.SESSION_COOKIE_BASKET_NAME])
        if len(cart_item) > 0:
            for item in cart_item:
                product = Trade.objects.get(id=item['id'])
                weight_item = PriceWeight.objects.get(id=item['weight_id'])
                price = ternary_operate(product.discount > 0,
                                        (weight_item.price - ((weight_item.price * product.discount) / 100)),
                                        weight_item.price
                                        )
                total_price += item['count'] * price

    if inside:
        return total_price
    return JsonResponse(dict(result=total_price))


def payment_preparing(request):
    form = PaymentForm(request.POST)
    params = {
        'form': form
    }
    if request.POST:
        params.update(dict(has_data=True))
        return render(request, 'app/payment.html', params)

    params.update(generate_view_params(request))
    return render(request, 'app/payment.html', params)


def pay_money(request):
    if request.POST:
        form = PaymentForm(request.POST)
        if form.is_valid():
            if settings.SESSION_COOKIE_BASKET_NAME in request.COOKIES:
                cart_items = json.loads(request.COOKIES[settings.SESSION_COOKIE_BASKET_NAME])
            else:
                cart_items = []

            score = 0
            for item in cart_items:
                product = Trade.objects.get(id=item['id'])
                weight_item = PriceWeight.objects.get(id=item['weight_id'])
                price = ternary_operate(product.discount > 0,
                                        (weight_item.price - ((weight_item.price * product.discount) / 100)),
                                        weight_item.price
                                        )
                cart_items[cart_items.index(item)]['weight'] = product.price_settings
                if product.discount > 0:
                    cart_items[cart_items.index(item)]['product'] = product
                score += item['count'] * price

            if len(cart_items) is 0:
                return HttpResponseRedirect('basket')

            t = loader.get_template('app/partial/_order_template.html')

            c = dict(basket_items=cart_items, total_price=score)
            content = t.render(c, request)

            t_email = loader.get_template('app/partial/_email_order.html')
            c_email = dict(
                basket_items=cart_items,
                total_price=score,
                name=form.cleaned_data['name'],
                phone=form.cleaned_data['phone'],
                address=form.cleaned_data['address']
            )
            t_content = t_email.render(c_email, request)
            thread = threading.Thread(target=send_email_notification,
                                      args=('Новый заказ', t_content, 'alinatrade01@gmail.com'))
            thread.start()

            order = Order()
            order.elements = content
            order.sum = count_basket_cost(request, True)
            order.save()

            return render(request, 'app/thanks.html')

    return HttpResponseRedirect('payment_prepare')


def send_email_notification(title, body, to):
    email = EmailMessage(title, body=body, to=[to])
    email.content_subtype = 'html'
    email.send()


@csrf_exempt
def temp_trade_add_view(request):
    if request.POST:
        product = Trade()
        title = request.POST.get('title').split(',')
        _category = request.POST.get('category')
        price = request.POST.get('price')
        article = request.POST.get('article')
        product_code = request.POST.get('product_code')

        _title = title[0]
        _description = ','.join('%s' % s for s in title if s is not _title).encode('utf-8')

        category_object = Category.objects.filter(title=_category).first()
        if category_object is None:
            _cat = Category()
            _cat.title = _category
            _cat.type = 'tools'
            _cat.save()
            category_object = _cat

        price = int(price)

        if PriceWeight.objects.filter(price=float(price), weight='-').first() is None:
            price_object = PriceWeight()
            price_object.weight = '-'.decode('utf-8')
            price_object.price = float(price)
            price_object.save()
        else:
            price_object = PriceWeight.objects.filter(price=float(price), weight='-').first()

        product.title = _title
        product.short_description = _description
        product.article = article
        product.product_code = product_code
        product.is_visible = True
        product.category = category_object
        product.brand = 'alinapaint'
        product.image = request.FILES['image']
        product.save()
        product.price_settings.add(price_object)
        return JsonResponse(dict(success=True, message='None'))

    return JsonResponse(dict(success=False, message='Request is GET'))


@csrf_exempt
def upload_trade_data(request):
    title = request.POST.get('title')
    short_description = request.POST.get('short_description')
    description = request.POST.get('description')
    instruction = request.POST.get('instruction')
    properties = request.POST.get('properties')
    brand = request.POST.get('brand')
    category = request.POST.get('category')

    if Trade.objects.filter(title=title, brand=brand).first() is not None:
        return JsonResponse(dict(success=True, message='already added'))

    trade = Trade()
    trade.title = title
    trade.short_description = short_description
    trade.description = description
    trade.instruction = instruction
    trade.properties = properties
    trade.is_visible = True
    trade.brand = brand
    trade.image = request.FILES['image']
    trade.category = Category.objects.filter(title=category).first()

    trade.save()

    return JsonResponse(dict(success=True))



