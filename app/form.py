from django import forms


class SearchForm(forms.Form):
    title = forms.CharField(required=True)


class PaymentForm(forms.Form):
    name = forms.CharField(max_length=255, required=True)
    phone = forms.CharField(max_length=255, required=True)
    address = forms.CharField(max_length=255, required=True)


class CalculatorForm(forms.Form):
    length = forms.IntegerField(required=False)
    weight = forms.IntegerField(required=False)
    height = forms.IntegerField(required=False)
    total_sum = forms.IntegerField(required=False)
