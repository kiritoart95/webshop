# coding=utf-8
import hashlib

from django import template

register = template.Library()


@register.filter
def get_price_with_discount(price, discount):
    price = float(price)
    discount = int(discount)

    return price - ((price * discount) / 100)


@register.filter
def get_object_count(array):
    return len(list(array))


@register.tag
def put_correct_finish(word, value):
    value = int(value)
    if value is 1 or value % 10 is 1:
        word += ''
    elif value in [2, 3, 4]:
        word += 'а'
    else:
        word += 'ов'

    return word


@register.filter
def encode_value(value):
    value = hashlib.md5(value)
    return value.hexdigest()
